const Wechat = {},
  XML2J = require('fast-xml-parser'),
  FXPJ = require('fast-xml-parser').j2xParser,
  J2XML = new FXPJ(),
  // request = require('request'),
  axios = require('axios'),
  randomApi = require('./random.api'),
  crypto = require('crypto'),
  md5 = require('MD5')

class PublicAccount {
  constructor () {
    console.log('initilize porject Generator')
  }
}

class Pay {
  constructor () {
    console.log('initilize porject Generator')
    // exec('')
    // config
    var sandbox = 'sandboxnew/'
    this.setUp = false
    // urls
    this.getSignKeyUrl = `https://api.mch.weixin.qq.com/${sandbox}pay/getsignkey`// 获取验签秘钥API：
    this.microPayUrl = `https://api.mch.weixin.qq.com/${sandbox}pay/micropay`// 付款码支付URL
    this.orderQueryUrl = `https://api.mch.weixin.qq.com/${sandbox}pay/orderquery`// 付款码支付URL
    this.downloadBillUrl = `https://api.mch.weixin.qq.com/${sandbox}pay/downloadbill`// 付款码支付URL
    // settings
    this.mch_id = null
    this.nonce_str = null
    this.sign = null
    this.appid = null
    this.device_info = null
    this.body = null
  }
  initilize (settings) {
    console.log(`initilizing with settings ${settings.mch_id}`)
    this.appid = settings.appid
    this.device_info = settings.device_info
    this.body = settings.body
    this.mch_id = settings.mch_id
    this.key = settings.key

    this.setUp = true
  }
  wrap (data) {
    return `<xml>${data}</xml>`
  }
  signData () {
    var stringA = `appid=${this.appid}&body=${this.body}&device_info=${this.device_info}&mch_id=${this.mch_id}&nonce_str=${this.nonce_str}`
    var stringSignTemp = stringA + `&key=${this.key}` // 注：key为商户平台设置的密钥key
    // sign = MD5(stringSignTemp).toUpperCase() = "9A0A8659F005D6984697E2CA0A9CF3B7" //注：MD5签名方式
    // this.sign = hash_hmac('sha256', stringSignTemp, key).toUpperCase() // 注：HMAC-SHA256签名方式
    console.log(`before encryption ${stringSignTemp}`)

    this.sign = md5(stringSignTemp).toUpperCase()
    // this.sign = crypto.createHmac('sha256', this.key).update(stringSignTemp).digest('hex')
  }

  microPay () {
    var form = { appid: this.appid, device_info: this.device_info, mch_id: this.mch_id, nonce_str: this.nonce_str, sign: this.sign }

    axios({
      method: 'post',
      url: this.getSignKeyUrl,
      data: form,
      config: {
        headers: { 'Content-Type': 'text/xml' }
      }
    })
      .then(function (response) {
        // handle success
        console.log(response.data)
      })
      .catch(function (response) {
        // handle error
        console.log(response.data)
      })
  }
  getSignKey () {
    this.nonce_str = 'ibuaiVcKdpRxkhJA'// randomApi.Strings.wxNonce32()
    this.signData()
    console.log(`nonce ${this.nonce_str}`)
    console.log(`sign ${this.sign}`)
    var form = { appid: this.appid, device_info: this.device_info, mch_id: this.mch_id, nonce_str: this.nonce_str, sign: this.sign }
    form = this.wrap(J2XML.parse(form))
    console.log(`xml is ${form}`)
    axios({
      method: 'post',
      url: this.getSignKeyUrl,
      data: form,
      config: {
        headers: { 'Content-Type': 'text/xml' }
      }
    })
      .then(function (response) {
        // handle success
        console.log(response.data)
      })
      .catch(function (response) {
        // handle error
        console.log(response.data)
      })
    /* request(this.getSignKeyUrl, { json: true },
      { form: { mch_id: this.mch_id, nonce_str: this.nonce_str, sign: this.sign } },
      (err, res, body) => {
        if (err) console.log(err)
        console.log(body)
      }) */
  }
}

Wechat.PublicAccount = new PublicAccount()
Wechat.Pay = new Pay()

module.exports = Wechat
