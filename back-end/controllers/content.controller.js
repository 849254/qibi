const Content = require('../models/content.model')
const Article = require('../models/articles.model')
const ArticleCategory = require('../models/article_categories.model')

exports.saveTermsOfService = async function (req, res) {
  await Content.updateTermsOfService(req.query.termsOfService)
  res.json({ success: true })
}

exports.updateAboutIntro = async function (req, res) {
  await Content.updateAboutIntro(req.query)
  res.json({ success: true })
}

exports.savePrivacyPolicy = async function (req, res) {
  await Content.updatePrivacyPolicy(req.query.privacyPolicy)
  res.json({ success: true })
}

exports.getAllContents = async function (req, res) {
  var contents = await Content.find()
  res.json({ success: true, contents: contents })
}

exports.getSpecificContent = async function (req, res) {
  console.log(`${req.query.page}`)
  var contents = await Content.find({ page: req.query.page })
  res.json({ success: true, contents: contents })
}
exports.addTeamMemberImg = async function (req, res) {
  res.json({ success: true })
}

exports.saveAllContents = async function (req, res) {
  var contents = await req.query.contents
  await Content.updateMany(contents)
  res.json({ success: true })
}

exports.updateMileStones = async function (req, res) {
  var mileSones = req.body.mileSones
  await Content.updateMileStones(mileSones)
  res.json({ success: true })
}

exports.updateTeamMembers = async function (req, res) {
  var teamMembers = req.body.teamMembers
  console.log(`${JSON.stringify(req.body.teamMembers)}`)
  await Content.updateTeamMembers(teamMembers)
  res.json({ success: true })
}

exports.getArticles = async function (req, res) {
  var result = await Article.getArticles(req.query)
  res.json(result)
}
exports.getOtherArticles = async function (req, res) {
  var result = await Article.getArticles(req.query)
  res.json(result)
}

exports.getArticle = async function (req, res) {
  var article = await Article.findOne({ _id: req.query.id })
  res.json({ success: true, article: article })
}

exports.getArticleCategories = async function (req, res) {
  var articleCategories = await ArticleCategory.find()
  res.json({ success: true, articleCategories: articleCategories })
}

exports.addArticleCategory = async function (req, res) {
  var articleCategory = await ArticleCategory.create(req.query)
  await articleCategory.save()
  res.json({ success: true })
}

exports.updateArticleCategory = async function (req, res) {
  await ArticleCategory.updateOne({
    _id: req.query._id,
    name: req.query.name
  })
  res.json({ success: true })
}

exports.deleteArticleCategory = async function (req, res) {
  await ArticleCategory.deleteOne({ _id: req.query._id })
  res.json({ success: true })
}

exports.addArticle = async function (req, res) {
  var result = Article.addArticle(req.query)
  res.json(result)
}

exports.updateArticle = async function (req, res) {
  var article = await req.query.article
  await Article.updateOne(article)
  res.json({ success: true })
}

exports.deleteArticle = async function (req, res) {
  await Article.deleteOne({ _id: req.query.id })
  res.json({ success: true })
}

exports.doLog = async function (req, res) {
  var log = req.query
  log.ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
  log.datetime = new Date(Date.now())
  await Content.doLog(log)
  res.json({ success: true })
}

exports.doGetLogs = async function (req, res) {
  console.log(`doGetLogs loging ...`)
  console.log(`request with params ${JSON.stringify(req.query)}`)
  var logs = await Content.doGetLogs(req.query)
  res.json({ success: true, logs: logs })
}

exports.saveSlides = async function (req, res) {
  console.log(`recived query ${JSON.stringify(req.query)}`)
  console.log(`recived body ${JSON.stringify(req.body)}`)
  await Content.saveSlides(req.body)
  res.json({ success: true })
}

exports.getPersonalHomeContent = async function (req, res) {
  console.log(`getPersonalHomeContent query is : ${JSON.stringify(req.query)}`)
  var stats = {}
  if (req.query.role_id == 1) {
    // get users countries
    // get each user role count
    // get approved tutors and waiting for aproval and thir total income
    // get courses.count and classses.count and total enrolled students
    // get financial things
    stats = {
      users_count: { admin_count: 54, tutor_count: 234, student_count: 3254 },
      tutor_stats: {
        qualified_tutors_count: 543,
        pending_qualification_count: 32432,
        toturs_total_income: 123
      },
      courses_stats: {
        total_courses_count: 3254,
        total_classes_count: 678,
        total_enrolled_student_count: 567
      },
      financial_stats: {
        total_income: 212,
        driven_income: 324,
        averge_user_driven_income: 3232
      }
    }
  } else if (req.query.role_id == 2) {
    stats = {
      tutor_stats: {
        myenrolled_students: 1,
        my_total_courses: 1,
        my_total_classes: 1,
        my_total_income: 1
      }
    }
  } else if (req.query.role_id == 3) {
    stats = {
      tutor_stats: {
        myenrolled_students: 1,
        my_total_courses: 1,
        my_total_classes: 1,
        my_total_income: 1
      }
    }
  }
  res.json({ success: true, stats: stats })
}
