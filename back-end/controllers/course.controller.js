const Course = require('../models/courses.model')
// const CourseCategory = require('../models/course_categories.model')
const CourseClass = require('../models/course_classes.model')
const ClassChats = require('../models/class_chats.model')
const User = require('../models/users.model')

exports.getHotCourses = async function (req, res) {
  var courses = await Course.find()
  res.json(courses)
}
exports.getCourseDetails = async function (req, res) { // classes
  var courseDetails = await Course.getCourseDetails(req.query.course_id)
  res.json({ success: true, courseDetails: courseDetails })
}

exports.getMyCoursesStats = function (req, res) {
  console.log('get My Enrollments')
  // var courses = await Course.getHotCourses()
  res.json({})
}
exports.doGenerateClassSlide = async function (req, res) {
  console.log(`from controller :${JSON.stringify(req.query)}`)
  var success = await CourseClass.generateClassSlides(req.query)
  res.json({ success: success })
}
exports.getMyEnrollmentsStats = function (req, res) {
  res.json({})
}

exports.getMyEnrollments = function (req, res) {
  res.json({})
}

exports.getMyCourses = async function (req, res) {
  console.log('get My Courses wit id :' + req.query.tutor_id)
  var courses = await Course.getMyCourses(req.query.tutor_id)
  res.json(courses)
}

exports.doGetCourseClasses = async function (req, res) { // classes
  var courses = await CourseClass.find({ course_id: req.query.course_id })
  res.json(courses)
}

exports.doGetCourseClassById = async function (req, res) { // classes
  var courseClass = await CourseClass.findOne({
    _id: req.query.class_id
  })
  res.json(courseClass)
}

exports.getCourseCategories = async function (req, res) {
  var courses = await CourseClass.find()
  res.json(courses)
}
exports.doGetRecentClasses = async function (req, res) {
  console.log('recived request')
  var classes = await CourseClass.find()
  console.log('classes are' + JSON.stringify(classes))
  res.json({ 'success': true, 'classes': classes })
}
exports.doGetCourseInit = async function (req, res) {
  var courses = ''
  if (req.session.role_id === 2) {
    courses = await Course.getMyCourses(req.session.user_id)
  } else {
    courses = await User.getMyEnrolledCourse(req.session.user_id)
  }
  res.json(courses)
}

exports.getCourseSlide = function (req, res) {
  var url = ''
  res.send(url)
}

exports.addNewCourse = async function (req, res) {
  var result = await Course.addCourse(req.query)
  res.json(result)
}

exports.addNewClass = async function (req, res) {
  var result = await CourseClass.addClass(req.query)
  res.json(result)
}

exports.updateCourse = function (req, res) {
  res.json({})
}

exports.updateClass = function (req, res) {
  res.json({})
}

exports.startNewClass = function (req, res) {
  res.json({})
}

exports.pauseClass = function (req, res) {
  res.json({})
}

exports.endClass = async function (req, res) {
  var result = await CourseClass.endClass(req.query)
  console.log(`result is xx ${result}`)
  res.json(result)
}

exports.removeClass = function (req, res) {
  res.json({})
}

exports.getMyFinacials = function (req, res) {
  res.json({})
}

exports.changeClassTo = function (req, res) {
  res.json({})
}

exports.sendClassMsg = async function (req, res) {
  console.log(`sendClassMsg sendClassMsg sendClassMsg`)
  var result = await ClassChats.addClassMsg(req.query)
  res.json(result)
}

exports.getClassChats = async function (req, res) {
  console.log(`requested !23`)
  var result = await ClassChats.getClassChats(req.query)
  console.log(`found ${JSON.stringify(result)}`)
  res.json(result)
}

exports.payForClass = function (req, res) {
  res.json({})
}

exports.enrollForCourse = async function (req, res) {
  await Course.enroll(req.query)
  res.json({ success: true })
}

exports.leaveCourse = async function (req, res) {
  await Course.leave(req.query)
  res.json({ success: true })
}

exports.getMyTimeTable = function (req, res) {
  res.json({})
}

exports.getMyEnrollmentsAndNewCourses = async function (req, res) {
  var newClasses = await CourseClass.find()
  var enrolledCourses = await Course.find({ is_approved: true }) // later change it
  res.json({ success: true, newClasses: newClasses, enrolledCourses: enrolledCourses })
}
