const User = require('../models/users.model')
const Course = require('../models/courses.model')
const Qualification = require('../models/qualifications.model')
const Notification = require('../models/notifications.model')

exports.doGetUserStats = function (req, res) {
  res.json({})
}

exports.doGetFinacialStats = function (req, res) {
  res.json({})
}

exports.doGetCoursesStat = function (req, res) {
  res.json({})
}

exports.deleteEmail = function (req, res) {
  res.json({})
}

exports.doGetMyEmails = function (req, res) {
  res.json({})
}

exports.sendEmail = function (req, res) {
  res.json({})
}

exports.composeEmail = function (req, res) {
  res.json({})
}

exports.doGetUsers = function (req, res) {
  User.find({}, function (err, result) {
    if (err) throw err
    console.log()
    res.json(result)
  })
}

exports.doDeleteUser = function (req, res) {
  res.json({})
}

exports.doBanUser = function (req, res) {
  res.json({})
}

exports.doGetTutors = async function (req, res) {
  var tutors = await User.find(req.query)
  for (var i = 0; i < tutors.length; i++) {
    tutors[i].courses = await Course.find({ 'tutor_id': tutors[i].id })
    tutors[i].course_count = tutors[i].courses.length
    for (var x = 0; x < tutors[i].courses.length; x++) {
      tutors[i].driven_income = tutors[i].driven_income + tutors[i].courses[x].driven_income
      tutors[i].personal_income = tutors[i].personal_income + tutors[i].courses[x].personal_income
      tutors[i].class_count = tutors[i].class_count + tutors[i].courses[x].classes.length
    }
  }
  res.json(tutors)
}

exports.doGetTutorQualifications = async function (req, res) {
  var qualifications = await Qualification.find(req.query)
  res.json(qualifications)
}

exports.toggolTutorQualification = async function (req, res) {
  console.log(`qualifiying ${req.query._id}`)
  var tutor = await User.findOne({ _id: req.query._id })
  tutor.is_approved = req.query.qualified
  await tutor.save()
  res.json({ success: true })
}

/* exports.doApproveTutor = async function (req, res) {
  var tutor = await User.findOne({ _id: req.query._id })
  tutor.is_approved = true
  await tutor.save()
  res.json({ success: true })
}

exports.doRevokeTutor = async function (req, res) {
  var tutor = await User.findOne({ _id: req.query._id })
  tutor.is_approved = false
  await tutor.save()
  res.json({ success: true })
} */

exports.doGetCourses = function (req, res) {
  Course.find({}, function (err, result) {
    if (err) throw err
    console.log()
    res.json(result)
  })
}

exports.toggoleCourseAproval = async function (req, res) {
  var course = await Course.findOne({ _id: req.query._id })
  course.is_approved = !course.is_approved
  await course.save()
  res.json({ success: true })
}

exports.doGetPaymentRecords = function (req, res) {
  res.json({})
}

exports.doGetPaymentsStats = function (req, res) {
  res.json({})
}

exports.doGetPageContent = function (req, res) {
  res.json({})
}

exports.doUpdatePageContent = function (req, res) {
  res.json({})
}

exports.doGetSmsRecords = function (req, res) {
  res.json({})
}

exports.doGetSmsStats = function (req, res) {
  res.json({})
}

exports.doGetWechatSettings = function (req, res) {
  res.json({})
}

exports.doSaveWechatSettings = function (req, res) {
  res.json({})
}

exports.doGetWechatMenu = function (req, res) {
  res.json({})
}

exports.doSaveWechatMenu = function (req, res) {
  res.json({})
}
