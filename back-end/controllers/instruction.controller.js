const Instruction = require('../models/instruction.model')

exports.getAllInstruction = async function (req, res) {
  console.log(`get all Instructions`)
  var instructions = await Instruction.find()
  res.json({ success: true, instructions: instructions })
}

exports.getInstruction = async function (req, res) {
  console.log(`get Instruction`)
  var instructions = await Instruction.find({ _id: req.query._id })
  res.json({ success: true, instructions: instructions })
}

exports.updateInstruction = async function (req, res) {
  console.log(`update Instruction`)
  console.log(`body : ${JSON.stringify(req.body)}`)
  await Instruction.updateOne({ _id: req.query._id }, { content: req.query.content })
  res.json({ success: true })
}
