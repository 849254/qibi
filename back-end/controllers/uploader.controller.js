/* eslint-disable eqeqeq */
const formidable = require('formidable'),
  fs = require('fs-extra'),
  global = require('../global'),
  Uploads = require('../models/uploads.model')

// exports.uploadFile = function (req, res) {
/* req.header('Access-Control-Allow-Origin', '*')
req.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
console.log('uploaded' + req.files)
const files = req.files
if (!files) {
const error = new Error('Please choose files')
error.httpStatusCode = 400
}
var paths = []
for (var i = 0; i < files.length; i++) {
paths.push(files[i].path.substring(files[i].path.lastIndexOf('/')))
}
res.json({ paths: paths }) */
// if user img
// if qualification img
// var courses = await Course.getHotCourses()
// req.files
// req.files is array of `photos` files
// req.body will contain the text fields, if there were any
// }
exports.getUploadById = async function (req, res) {
  console.log(`req.query.uploadID :${req.query.uploadID} `)
  var upload = await Uploads.getUploadById(req.query.uploadID)
  res.json({ success: true, upload: upload })
}
exports.uploadFile = async function (req, res) {
  console.clear()
  var userID, uploadID, subID = ''
  var filePath, fileName, url = ''
  const form = new formidable.IncomingForm()
  const URLs = []
  form.parse(req, (_, fields, files) => {
    userID = fields['userID']
    uploadID = fields['uploadID']
    subID = fields['subID']
    Object.keys(files).forEach(async function (key, index) {
      var file = files[key]
      // console.log(`processing file :${JSON.stringify(file)}`)
      // console.log(`with fields :${JSON.stringify(fields)}`)
      if (uploadID == 1 || uploadID == 2) {
        // console.log(`uploadPath: ${global.uploadPath}`)
        filePath = global.uploadPath + '/contents'
      } else if (uploadID == 'personal_img') {
        filePath = global.uploadPath + '/personalImgs'
      } else if (uploadID == 'scannedImg') {
        filePath = global.uploadPath + '/scannedImg'
      }
      // console.log(`uploadID :${uploadID} , filePath: ${filePath}`)
      fs.ensureDirSync(filePath)
      fileName = files[key].name
      fs.renameSync(file.path, filePath + '/' + fileName)
      url = filePath.substring(global.uploadPath.lastIndexOf('/'), filePath.length) + '/' + fileName
      console.log(`URL is ${url}`)
      URLs.push(url)
      console.log(`URLs are ${JSON.stringify(URLs)}`)
      var upload = {
        subID: subID,
        userID: userID,
        uploadID: uploadID,
        fileName: fileName,
        filePath: filePath,
        uploadDir: filePath,
        url: url
      }
      console.log(`upload ${JSON.stringify(upload)}`)
      await Uploads.addUpload(upload)
    })
    res.json({ success: true, URLs: URLs })
  })
}
