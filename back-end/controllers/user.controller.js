const User = require('../models/users.model'),
  PhoneVerification = require('../models/phone_verifications.model'),
  Notifcation = require('../models/notifications.model'),
  Qualification = require('../models/qualifications.model'),
  https = require('https'),
  querystring = require('querystring')

var success, msg, result

exports.getSession = function (req, res) {
  req.session.app_version = '1.0.0'
  var session = req.session
  res.json(session)
}

exports.doLogin = async function (req, res) {
  console.log(`do log in ${JSON.stringify(req.query)}`)
  var result = await User.doLogin(req.query)
  req.session.user_id = result.user_id
  req.session.role_id = result.role_id
  req.session.avatar_url = result.avatar_url
  req.session.username = result.username
  res.json(result)
}

exports.doChangePassword = async function (req, res) {
  success = false
  msg = '目前密码不正确'
  console.log(`doChangePassword query ${JSON.stringify(req.query)}`)
  var user = await User.findOne({ _id: req.query.user_id })
  user.password = req.query.new_password
  await user.save()
  success = true
  msg = '成功'
  res.json({ success: success, msg: msg })
}
exports.addNotification = async function (req, res) {
  console.log(`do log in ${JSON.stringify(req.query)}`)
  result = await Notifcation.find({})
  req.session.user_id = result.user_id
  req.session.role_id = result.role_id
  req.session.avatar_url = result.avatar_url
  req.session.username = result.username
  res.json(result)
}

exports.readNotification = async function (req, res) {
  await Notifcation.readNotification(req.query)
  res.json({ success: true })
}

exports.getAllMyNotifications = async function (req, res) {
  result = await Notifcation.getAllMyNotifications(req.query.user_id)
  res.json(result)
}

exports.getMyNewNotifications = async function (req, res) {
  result = await Notifcation.getMyNewNotifications(req.query.user_id)
  res.json(result)
}
exports.doLogout = async function (req, res) {
  console.log('recived logout request')
  req.session.destroy()
  res.sendStatus(200)
}

exports.doVerifyName = async function (req, res) {
  result = await User.verifyUsername(req.query.username)
  res.json(result)
}

exports.doSignup = async function (req, res) {
  result = await User.signup(req.query)
  res.json(result)
}

exports.doRest = function (req, res) {
  res.json({})
}

exports.doVerifyPhone = async function (req, res) {
  var result
  if (req.query.code) {
    result = await PhoneVerification.verifCode(req.query.code, req.query.phone_number)
  } else {
    result = await User.verifyPhonenumber(req.query.phone_number)
  }
  res.json(result)
}

exports.doSendCode = async function (req, res) {
  var mobileNumber = req.query.phonenumber
  console.log(`phone number is : ${mobileNumber}`)
  var code = await PhoneVerification.generateCode(mobileNumber)
  console.log(`code is : ${code}`)
  if (process.platform === 'linux') {
    var postData = querystring.stringify({
      'apikey': 'c76b9b8bb2ec8fe405cecd15150d05ce',
      'mobile': `${mobileNumber}`,
      'text': `【大德汇】您的验证码是${code}`,
      'register': 'false'
    })

    console.log(`Post Data is : ${postData}`)
    console.log('Updating...')

    var options = {
      hostname: 'sms.yunpian.com',
      port: 443,
      path: '/v2/sms/single_send.json',
      method: 'POST',
      headers: {
        'Accept': 'application/json;charset=utf-8',
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
        'Content-Length': Buffer.byteLength(postData)
      }
    }
    let data = ''
    var requ = https.request(options, (resp) => {
      resp.on('data', (chunk) => {
        data += chunk
        console.log(`Recived Chunk : ${chunk}`)
      })
      resp.on('end', () => {
        console.log(`final data is  ${JSON.parse(data)}`)
      })
    }).on('error', (err) => {
      console.log('Error:' + err.message)
    })
    await requ.write(postData)
    console.log('postData :')
    console.log(postData)
    console.log('data :')
    console.log(data)

    requ.end()
    res.json(JSON.parse(data))
  }
  success = true
  res.json({ 'msg': '发送成功', 'success': success })
}

exports.doGetMyPersonalInfo = async function (req, res) {
  var me = await User.findOne({ _id: req.query.user_id })
  res.json({ basicInfo: me, success: true })
}

exports.doSavePersonalInfo = async function (req, res) {
  await User.updateOne({ _id: req.body.info._id }, req.body.info)
  res.json({ success: true })
}

exports.doGetMyQualifications = async function (req, res) {
  console.log(`requesting with id ${req.query.tutor_id}`)
  var qualifications = await Qualification.find({ tutor_id: req.query.tutor_id, is_deleted: false })
  res.json(qualifications)
}

exports.doAddQualification = async function (req, res) {
  result = await Qualification.addQualification(req.query)
  res.json(result)
}

exports.doDeleteQualification = function (req, res) {
  res.json({})
}

exports.doUpdateQualification = function (req, res) {
  res.json({})
}

exports.isQualifiedTutor = async function (req, res) {
  console.log(`********* ${req.query.user_id}`)
  var tutor = await User.findOne({ _id: req.query.user_id })
  console.log('got tutor' + tutor.is_approved)
  res.json({ qualified: tutor.is_approved })
}

exports.doMessageUser = function (req, res) {
  res.json({})
}

exports.doGetMessages = function (req, res) {
  res.json({})
}

exports.doGetNotifications = function (req, res) {
  res.json({})
}

exports.doGetAlerts = function (req, res) {
  res.json({})
}
