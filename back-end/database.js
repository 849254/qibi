var mongoose = require('mongoose')
var ObjectID = require('mongodb').ObjectID
const Role = require('./models/roles.model')
const User = require('./models/users.model')
const Qualification = require('./models/qualifications.model')
const ArticleCategory = require('./models/article_categories.model')
const Article = require('./models/articles.model')

const Course = require('./models/courses.model')
const Content = require('./models/content.model')
const CourseClass = require('./models/course_classes.model')
const CourseCategory = require('./models/course_categories.model')
const Enrollment = require('./models/enrollments.model')
const Notification = require('./models/notifications.model')
const Instrucion = require('./models/instruction.model')
const fs = require('fs-extra')
const globals = require('./global')

// var env = process.env.NODE_ENV || 'dev'
var seed = true

module.exports = class db {
  constructor() {
    this.db = null
    console.log(``)
    // if (env === 'dev') {
    this.connectionString = 'mongodb://localhost:27017/ibiplus'
    // }
  }

  async init () {
    console.log('Initalizing Database')
    // mongoose.set('debug', true);
    // this.db = await mongoose.connect('mongodb://localhost:27017/ibiplus', { useNewUrlParser: true })
    mongoose.connect(this.connectionString)
    this.db = mongoose.connection
    this.db.on('error', console.error.bind(console, 'connection error:'))
    let self = this
    this.db.once('open', async function () {
      console.log('Connection with database succeeded.')
      if (seed) {
        const roles = [
          { roleName: 'admin' },
          { roleName: 'tutor' },
          { roleName: 'student' }
        ]
        /*
        '' ss
        ''
        ''
        '5d366e3a2f7320ebfc94cfc5'
        '5d366e3a2f7320ebfc94cfc6'
        '5d366e3a2f7320ebfc94cfc7'
        '5d366e3a2f7320ebfc94cfc8'
        '5d366e3a2f7320ebfc94cfc9'
        '5d366e3a2f7320ebfc94cfca'
        '5d366e3a2f7320ebfc94cfcb'
        '5d366e3a2f7320ebfc94cfcc'
        '5d366e3a2f7320ebfc94cfcd'
        '5d366e3a2f7320ebfc94cfce'
        '5d366e3a2f7320ebfc94cfcf'
        '5d366e3a2f7320ebfc94cfd0' */

        var user1Id = '5d366e3a2f7320ebfc94cfc6'
        var user2Id = ObjectID('5d366e3a2f7320ebfc94cfa0')
        var user3Id = ObjectID('5d366e3a2f7320ebfc94cfa1')
        var user4Id = ObjectID('5d366e3a2f7320ebfc94cfa2')
        var user5Id = ObjectID('5d366e3a2f7320ebfc94cfa3')
        var user6Id = ObjectID('5d366e3a2f7320ebfc94cfa4')
        var user7Id = ObjectID('5d366e3a2f7320ebfc94cfa5')
        var user8Id = ObjectID('5d366e3a2f7320ebfc94cfa6')
        var user9Id = ObjectID('5d366e3a2f7320ebfc94cfa7')

        const users = [
          { _id: user1Id, phone: '13066671453', username: 'admin1', avatar_url: 'http://thirdwx.qlogo.cn/mmopen/0T8yO33zeejENICqDPAva0AkwvxaHCFCguib01Ms3BibHElj1TgpFDCHkdxTpNyefNxU6t0ibboXhibJjtn5BDGiaPA/132', password: 'admin1', role: 1 },
          { _id: user2Id, username: 'admin2', avatar_url: 'statics/img/user_avatar.jpg', password: 'admin2', role: 1 },
          { _id: user3Id, username: 'admin3', avatar_url: 'statics/img/user_avatar.jpg', password: 'admin3', role: 1 },
          { _id: user4Id, username: 'tutor1', is_approved: true, avatar_url: 'statics/img/user_avatar.jpg', password: 'tutor1', role: 2, courses: [] },
          { _id: user5Id, username: 'tutor2', avatar_url: 'statics/img/user_avatar.jpg', password: 'tutor2', role: 2, courses: [] },
          { _id: user6Id, username: 'tutor3', avatar_url: 'statics/img/user_avatar.jpg', password: 'tutor3', role: 2, courses: [] },
          { _id: user7Id, username: 'student1', avatar_url: 'statics/img/user_avatar.jpg', password: 'student1', role: 3 },
          { _id: user8Id, username: 'student2', avatar_url: 'statics/img/user_avatar.jpg', password: 'student2', role: 3 },
          { _id: user9Id, username: 'student3', avatar_url: 'statics/img/user_avatar.jpg', password: 'student3', role: 3 }
        ]
        var notification1Id = ObjectID('5d366e3a2f7320ebfc94cfa8')
        var notification2Id = ObjectID('5d366e3a2f7320ebfc94cfa9')
        var notification3Id = ObjectID('5d366e3a2f7320ebfc94cfaa')

        const notifications = [
          { _id: notification1Id, user_id: user1Id, text: '欢迎光临', link: '/', type: 1, img_url: '', is_read: false },
          { _id: notification2Id, user_id: user1Id, text: '欢迎光临', link: '/personalcenter', type: 2, img_url: '', is_read: false },
          { _id: notification3Id, user_id: user1Id, text: '欢迎光临', link: '/login', type: 3, img_url: '', is_read: false }
        ]

        var qualification1Id = ObjectID('5d366e3a2f7320ebfc94cfab')
        var qualification2Id = ObjectID('5d366e3a2f7320ebfc94cfac')
        var qualification3Id = ObjectID('5d366e3a2f7320ebfc94cfad')
        var qualification4Id = ObjectID('5d366e3a2f7320ebfc94cfae')
        var qualification5Id = ObjectID('5d366e3a2f7320ebfc94cfaf')
        var qualification6Id = ObjectID('5d366e3a2f7320ebfc94cfb0')
        var qualification7Id = ObjectID('5d366e3a2f7320ebfc94cfb1')

        const qualifications = [
          { _id: qualification1Id, tutor_id: user4Id, type: 'accomplishment', institiuation_name: 'MIT', institiuation_country: 'USA', institiuation_state: 'California', institiuation_city: 'Los Angelous', institiuation_contact_information: '2234534325434', date_of_aquiry: Date.now(), scanned_img_url: ['http://localhost:5000/uploads/1563926007430_screen.png'] },
          { _id: qualification2Id, tutor_id: user4Id, type: 'diploma', institiuation_name: 'MIT', institiuation_country: 'USA', institiuation_state: 'California', institiuation_city: 'Los Angelous', institiuation_contact_information: '2234534325434', date_of_aquiry: Date.now(), scanned_img_url: ['http://localhost:5000/uploads/1563926007430_screen.png'] },
          { _id: qualification3Id, tutor_id: user4Id, type: 'bachelor_degree', institiuation_name: 'MIT', institiuation_country: 'USA', institiuation_state: 'California', institiuation_city: 'Los Angelous', institiuation_contact_information: '2234534325434', date_of_aquiry: Date.now(), scanned_img_url: ['http://localhost:5000/uploads/1563926007430_screen.png'] },
          { _id: qualification4Id, tutor_id: user5Id, type: 'master_degree', institiuation_name: 'MIT', institiuation_country: 'USA', institiuation_state: 'California', institiuation_city: 'MIT', institiuation_contact_information: '2234534325434', date_of_aquiry: Date.now(), scanned_img_url: ['http://localhost:5000/uploads/1563926007430_screen.png'] },
          { _id: qualification5Id, tutor_id: user5Id, type: 'phd', institiuation_name: 'MIT', institiuation_country: 'USA', institiuation_state: 'California', institiuation_city: 'Los Angelous', institiuation_contact_information: '2234534325434', date_of_aquiry: Date.now(), scanned_img_url: ['http://localhost:5000/uploads/1563926007430_screen.png'] },
          { _id: qualification6Id, tutor_id: user6Id, type: 'master_degree', institiuation_name: 'MIT', institiuation_country: 'USA', institiuation_state: 'California', institiuation_city: 'Los Angelous', institiuation_contact_information: '2234534325434', date_of_aquiry: Date.now(), scanned_img_url: ['http://localhost:5000/uploads/1563926007430_screen.png'] },
          { _id: qualification7Id, tutor_id: user6Id, type: 'phd', institiuation_name: 'MIT', institiuation_country: 'USA', institiuation_state: 'California', institiuation_city: 'Los Angelous', institiuation_contact_information: '2234534325434', date_of_aquiry: Date.now(), scanned_img_url: ['http://localhost:5000/uploads/1563926007430_screen.png'] }
        ]

        var category1Id = '5d366e3a2f7320ebfc94cfc2'
        var category2Id = '5d366e3a2f7320ebfc94cfc3'
        var category3Id = '5d366e3a2f7320ebfc94cfc4'

        const courseCategories = [
          { _id: category1Id, name: 'category 1', description: 'scince' },
          { _id: category2Id, name: 'category 2', description: 'language' },
          { _id: category3Id, name: 'category 3', description: 'economic' }
        ]
        var course1Id = ObjectID('5d366e3a2f7320ebfc94cfb3')
        var course2Id = ObjectID('5d366e3a2f7320ebfc94cfb4')
        var course3Id = ObjectID('5d366e3a2f7320ebfc94cfb5')

        const courses = [
          { _id: course1Id, img_url: 'statics/ph2.jpg', is_approved: true, tutor_id: user4Id, name: 'course 1', description: 'mathimatics' },
          { _id: course2Id, img_url: 'statics/ph2.jpg', tutor_id: user5Id, name: 'course 2', description: 'chemistry' },
          { _id: course3Id, img_url: 'statics/ph2.jpg', tutor_id: user6Id, name: 'course 3', description: 'physics' }
        ]
        var enrolment1Id = ObjectID('5d366e3a2f7320ebfc94cfb6')
        var enrolment2Id = ObjectID('5d366e3a2f7320ebfc94cfb7')
        var enrolment3Id = ObjectID('5d366e3a2f7320ebfc94cfb8')
        var enrolment4Id = ObjectID('5d366e3a2f7320ebfc94cfb9')
        var enrolment5Id = ObjectID('5d366e3a2f7320ebfc94cfba')
        var enrolment6Id = ObjectID('5d366e3a2f7320ebfc94cfbb')

        const enrollments = [
          { _id: enrolment1Id, user_id: user7Id, course_id: course1Id },
          { _id: enrolment2Id, user_id: user7Id, course_id: course2Id },
          { _id: enrolment3Id, user_id: user7Id, course_id: course3Id },
          { _id: enrolment4Id, user_id: user8Id, course_id: course1Id },
          { _id: enrolment5Id, user_id: user8Id, course_id: course2Id },
          { _id: enrolment6Id, user_id: user9Id, course_id: course1Id }
        ]

        var courseClass1Id = ObjectID('5d366e3a2f7320ebfc94cfbc')
        // var courseClass2Id = ObjectID('5d366e3a2f7320ebfc94cfbd')
        /* var courseClass3Id = ObjectID('5d366e3a2f7320ebfc94cfbe')
        var courseClass4Id = ObjectID('5d366e3a2f7320ebfc94cfbf')
        var courseClass5Id = ObjectID('5d366e3a2f7320ebfc94cfc0')
        var courseClass6Id = ObjectID('5d366e3a2f7320ebfc94cfc1') */

        const courseClasses = [
          { _id: courseClass1Id, course_id: course1Id, name: 'course 1 class 1', description: 'Intro', class_file_url: '/sample1.ppt', video_recorded: false, img_url: 'statics/ph2.jpg' }// ,
          // { _id: courseClass2Id, course_id: course1Id, name: 'course 1 class 2', description: 'Details', class_file_url: '/sample2.ppt' }
          /*, { _id: courseClass3Id, course_id: course1Id, name: 'course 1 class 3', description: 'Expand', class_file_url: '/sample3.ppt' },
          { _id: courseClass4Id, course_id: course2Id, name: 'course 2 class 1', description: 'Intro', class_file_url: '/sample4.ppt' },
          { _id: courseClass5Id, course_id: course2Id, name: 'course 2 class 2', description: 'Details', class_file_url: '/sample5.ppt' },
          { _id: courseClass6Id, course_id: course3Id, name: 'course 3 class 1', description: 'Intro', class_file_url: '/sample6.ppt' } */
        ]
        var contentId1 = ObjectID('5d366e3a2f7320ebfc94cfc5')
        var contentId2 = ObjectID('5d366e3a2f7320ebfc94cfc6')
        var contentId3 = ObjectID('5d366e3a2f7320ebfc94cfc7')
        var contentId4 = ObjectID('5d366e3a2f7320ebfc94cfc8')
        var contentId5 = ObjectID('5d366e3a2f7320ebfc94cfc9')
        var contentId6 = ObjectID('5d366e3a2f7320ebfc94cfca')
        var contentId7 = ObjectID('5d366e3a2f7320ebfc94cfcb')
        var contentId8 = ObjectID('5d366e3a2f7320ebfc94cfcc')
        var contentId9 = ObjectID('5d366e3a2f7320ebfc94cfcd')
        var contentId10 = ObjectID('5d366e3a2f7320ebfc94cfce')
        var contentId11 = ObjectID('5d366e3a2f7320ebfc94cfcf')
        var contentId12 = ObjectID('5d366e3a2f7320ebfc94cfc4')
        var contentId13 = ObjectID('5d366e3b2f7320ebfc94cfc3')

        const contents = [
          {
            '_id': contentId1,
            'name': 'milestones',
            'page': 'about',
            'obj': {
              'Array': [
                { id: 1, title: 'one ', date: '2019/01/01', content: 'some content 1', edit: false, new: false },
                { id: 2, title: 'tow ', date: '2019/01/02', content: 'some content 2', edit: false, new: false },
                { id: 3, title: 'three ', date: '2019/01/03', content: 'some content 3', edit: false, new: false },
                { id: 4, title: 'four ', date: '2019/01/04', content: 'some content 4', edit: false, new: false }
              ]
            }
          },
          {
            '_id': contentId2,
            'name': 'intro',
            'page': 'about',
            'obj': {
              'Content': 'IBIPlus is Greate',
              'URL': 'statics/ibiplusicon.png'
            }
          },
          {
            '_id': contentId3,
            'name': 'team',
            'page': 'about',
            'obj': {
              'Team': [
                { id: 1, img_url: 'statics/pph1.png', name: 'Team Member 1', description: 'Description 1' },
                { id: 2, img_url: 'statics/pph1.png', name: 'Team Member 2', description: 'Description 2' },
                { id: 3, img_url: 'statics/pph1.png', name: 'Team Member 3', description: 'Description 3' },
                { id: 4, img_url: 'statics/pph1.png', name: 'Team Member 4', description: 'Description 4' }
              ]
            }
          },
          {
            '_id': contentId4,
            'name': 'outstandingTutors',
            'page': 'tutors',
            'obj': {
              'Tutors': [
                { img_url: 'statics/pph1.png', name: 'Team Member 1', country: 'china', comments: [{ id: 1, body: 'Very Good Tutors', username: 'avatar 1' }, { body: 'Very Good Tutors', username: 'avatar 1' }, { body: 'Very Good Tutors', username: 'avatar 1' }] },
                { img_url: 'statics/pph1.png', name: 'Team Member 2', country: 'china', comments: [{ id: 2, body: 'Very Good Tutors', username: 'avatar 1' }, { body: 'Very Good Tutors', username: 'avatar 1' }, { body: 'Very Good Tutors', username: 'avatar 1' }] },
                { img_url: 'statics/pph1.png', name: 'Team Member 3', country: 'china', comments: [{ id: 3, body: 'Very Good Tutors', username: 'avatar 1' }, { body: 'Very Good Tutors', username: 'avatar 1' }, { body: 'Very Good Tutors', username: 'avatar 1' }] },
                { img_url: 'statics/pph1.png', name: 'Team Member 4', country: 'china', comments: [{ id: 4, body: 'Very Good Tutors', username: 'avatar 1' }, { body: 'Very Good Tutors', username: 'avatar 1' }, { body: 'Very Good Tutors', username: 'avatar 1' }] }
              ]
            }
          },
          {
            '_id': contentId5,
            'name': 'coursesIntro',
            'page': 'courses',
            'obj': {
              'coursesIntro': 'coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro coursesIntro '
            }
          },
          {
            '_id': contentId6,
            'name': 'coursesCategories',
            'page': 'courses',
            'obj': {
              'coursesCategories': [
                { id: 1, name: 'course cat 1', description: 'niceejnkjvnektjn krjntjenktjnkejrnk' },
                { id: 2, name: 'course cat 2', description: 'niceejnkjvnektjn krjntjenktjnkejrnk' },
                { id: 3, name: 'course cat 3', description: 'niceejnkjvnektjn krjntjenktjnkejrnk' },
                { id: 4, name: 'course cat 4', description: 'niceejnkjvnektjn krjntjenktjnkejrnk' }
              ]
            }
          },
          {
            '_id': contentId7,
            'name': 'caroselSlides',
            'page': 'home',
            'obj': {
              'caroselSlides': [
                { id: 1, name: 'slide1', img_url: 'statics/ph1.png', contents: [{ id: 1, text: 'text 1', type: 'title', position: '' }] },
                { id: 2, name: 'slide2', img_url: 'statics/ph1.png', contents: [{ id: 1, text: 'text 1', type: 'title', position: '' }] },
                { id: 3, name: 'slide3', img_url: 'statics/ph1.png', contents: [{ id: 1, text: 'text 1', type: 'title', position: '' }] },
                { id: 4, name: 'slide4', img_url: 'statics/ph1.png', contents: [{ id: 1, text: 'text 1', type: 'title', position: '' }] },
                { id: 5, name: 'slide5', img_url: 'statics/ph1.png', contents: [{ id: 1, text: 'text 1', type: 'title', position: '' }] },
                { id: 6, name: 'slide6', img_url: 'statics/ph1.png', contents: [{ id: 1, text: 'text 1', type: 'title', position: '' }] }
              ]
            }
          },
          {
            '_id': contentId8,
            'name': 'homePics',
            'page': 'home',
            'obj': {
              'homePics': [
                'statics/ph2.jpg',
                'statics/ph2.jpg',
                'statics/ph2.jpg',
                'statics/ph2.jpg'
              ]
            }
          },
          {
            '_id': contentId9,
            'name': 'homeTutors',
            'page': 'home',
            'obj': {
              'homeTutors': [
                { id: 1, img_url: 'statics/ph2.jpg', title: 'title 1', decription: 'description 1' },
                { id: 2, img_url: 'statics/ph2.jpg', title: 'title 2', decription: 'description 2' },
                { id: 3, img_url: 'statics/ph2.jpg', title: 'title 3', decription: 'description 3' },
                { id: 4, img_url: 'statics/ph2.jpg', title: 'title 4', decription: 'description 4' },
                { id: 5, img_url: 'statics/ph2.jpg', title: 'title 5', decription: 'description 5' }
              ]
            }
          },
          {
            '_id': contentId10,
            'name': 'hotCourses',
            'page': 'home',
            'obj': {
              'hotCourses': []
            }
          }, {
            '_id': contentId11,
            'name': 'privacyPolicy',
            'page': 'privacyPolicy',
            'obj': {
              'privacyPolicy': 'privacyPolicy privacyPolicy privacyPolicy privacyPolicy privacyPolicy'
            }
          }, {
            '_id': contentId12,
            'name': 'termsOfService',
            'page': 'termsOfService',
            'obj': {
              'termsOfService': 'termsOfService termsOfService termsOfService termsOfService termsOfServicetermsOfService'
            }
          }, {
            '_id': contentId13,
            'name': 'logs',
            'page': 'logs',
            'obj': {
              'logs': []
            }
          }
        ]

        var articleCategoryId1 = ObjectID('5d366e3a2f7320ebfc94cfcd')
        var articleCategoryId2 = ObjectID('5d366e3a2f7320ebfc94cfce')

        const articleCategories = [{ _id: articleCategoryId1, name: 'cat 1' }, { _id: articleCategoryId2, name: 'cat 2' }]

        var articleId1 = ObjectID('5d366e3a2f7320ebfc94cfcf')
        var articleId2 = ObjectID('5d366e3a2f7320ebfc94cfd0')

        const articles = [{
          _id: articleId1,
          title: 'article 1',
          content: `Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus magnam, debitis similique labore blanditiis maxime! Nulla, explicabo dolorem ab beatae aut ipsam eius officiis, accusantium, tempore unde veritatis ea neque.
  Lorem ipsum, dolor sit amet consectetur adipisicing elit.Minus magnam, debitis similique labore blanditiis maxime! Nulla, explicabo dolorem ab beatae aut ipsam eius officiis, accusantium, tempore unde veritatis ea neque.
  Lorem ipsum, dolor sit amet consectetur adipisicing elit.Minus magnam, debitis similique labore blanditiis maxime! Nulla, explicabo dolorem ab beatae aut ipsam eius officiis, accusantium, tempore unde veritatis ea neque.
  `,
          abstract: 'tgjkentkekgjtnejjetktnkerjk',
          author_id: '5d366e3a2f7320ebfc94cfc6',
          category_id: articleCategoryId1
        }, {
          _id: articleId2,
          title: 'articel 2',
          content: `Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus magnam, debitis similique labore blanditiis maxime! Nulla, explicabo dolorem ab beatae aut ipsam eius officiis, accusantium, tempore unde veritatis ea neque.
  Lorem ipsum, dolor sit amet consectetur adipisicing elit.Minus magnam, debitis similique labore blanditiis maxime! Nulla, explicabo dolorem ab beatae aut ipsam eius officiis, accusantium, tempore unde veritatis ea neque.
  Lorem ipsum, dolor sit amet consectetur adipisicing elit.Minus magnam, debitis similique labore blanditiis maxime! Nulla, explicabo dolorem ab beatae aut ipsam eius officiis, accusantium, tempore unde veritatis ea neque.
  `,
          abstract: 'temn tnrttn ',
          author_id: '5d366e3a2f7320ebfc94cfc6',
          category_id: articleCategoryId2
        }]
        var instrutctionId1 = ObjectID('4d366e3a2f7322ebfc94cfcf')
        var instrutctionId2 = ObjectID('4d366e3a2f7310ebfc94cfd0')
        const instrutction = [
          { _id: instrutctionId1, title: 'user managment', group: 'admin', content: '<p>one</p>' },
          { _id: instrutctionId2, title: 'user managment', group: 'admin', content: '<p>two</p>' }
        ]
        console.log('Cleaning Database')
        await this.db.listCollections().forEach(function (collection) {
          console.log('collection.name : ' + collection.name)
          // if (collection.name !== 'instrutctions') {
          self.db.collection(collection.name).drop()
          // }
        })

        console.log('Cleaning Directories')
        // fs.emptyDirSync(globals.uploadPath)
        fs.emptyDirSync(globals.slidesPath)
        console.log('Seeding Database\n')

        //  console.log('Creating Roles')
        await roles.forEach(function (role) {
          var newRole = new Role(role)
          newRole.save()
          // console.log(newRole.id)
        })
        //  console.log('Creating Users')
        await users.forEach(function (user) {
          var newUser = new User(user)
          newUser.save()
          // console.log(newUser.id)
        })

        //  console.log('Creating Notification')
        await notifications.forEach(function (notification) {
          var newnotification = new Notification(notifications)
          newnotification.save()
          // console.log(newnotification.id)
        })

        //  console.log('Creating Qualification')
        await qualifications.forEach(function (qualification) {
          var newQualification = new Qualification(qualification)
          newQualification.save()
          // console.log(newQualification.id)
        })

        //  console.log('Creating Courses')
        await courseCategories.forEach(function (courseCategory) {
          var newCourseCategory = new CourseCategory(courseCategory)
          newCourseCategory.save()
          // console.log(newCourse.id)
        })
        await courses.forEach(function (course) {
          var newCourse = new Course(course)
          newCourse.save()
          // console.log(newCourse.id)
        })

        //  console.log('Creating Enrollment')
        await enrollments.forEach(function (enrollment) {
          var newEnrollment = new Enrollment(enrollment)
          newEnrollment.save()
          // console.log(newEnrollment.id)
        })

        //  console.log('Creating Course Classes')
        await courseClasses.forEach(function (courseClass) {
          var newCourseClass = new CourseClass(courseClass)
          newCourseClass.save()
          // console.log(newCourseClass.id)
        })

        await contents.forEach(function (content) {
          var newContent = new Content(content)
          newContent.save()
          // console.log(newCourseClass.id)
        })

        await articleCategories.forEach(function (articleCategory) {
          var newArticleCategory = new ArticleCategory(articleCategory)
          newArticleCategory.save()
          // console.log(newCourseClass.id)
        })

        await articles.forEach(function (artilce) {
          var newArticle = new Article(artilce)
          newArticle.save()
          // console.log(newCourseClass.id)
        })

        await instrutction.forEach(function (instrutction) {
          var newInstrutction = new Instrucion(instrutction)
          newInstrutction.save()
          // console.log(newCourseClass.id)
        })
      }
    })
  }
}
