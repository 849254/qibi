const mongoose = require('mongoose')
let articleCategorySchema = new mongoose.Schema({
  name: { type: String }
})

module.exports = mongoose.model('ArticleCategory', articleCategorySchema)
