const mongoose = require('mongoose'),
  users = require('./users.model'),
  article_categories = require('./article_categories.model')

let articleSchema = new mongoose.Schema({
  title: { type: String },
  img_url: { type: String, default: 'statics/ph2.jpg' },
  content: { type: String },
  abstract: { type: String },
  author_id: { type: String },
  category_id: { type: String },
  username: { type: String },
  avatar_url: { type: String },
  category_name: { type: String },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})
articleSchema.index({ title: 'text', abstract: 'text' })

articleSchema.statics.getArticles = async function (params) {
  /* var articles = await this.aggregate([
    {
      $lookup:
      {
        from: 'User',
        localField: 'author_id',
        foreignField: 'identifer',
        as: 'author'
      }
    }, {
      $lookup:
      {
        from: 'ArticleCategory',
        localField: 'category_id',
        foreignField: '_id',
        as: 'cat'
      }
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$author', 0] }, '$$ROOT'] } }
    }
  ]) */
  console.log(`getting articles 1`)

  var skip = (params.current_page - 1) * params.per_page
  var query = params.category_id == -1 ? {} : { category_id: params.category_id }
  var articles = await this.find(query)
  var total = await articles.length
  articles = await this.find(query).skip(skip).limit(parseInt(params.per_page))
  console.log(`getting articles 2`)

  for (var i = 0; i < articles.length; i++) {
    var user = await users.findOne({ _id: articles[i].author_id })
    var articleCategory = await article_categories.findOne({ _id: articles[i].category_id })
    articles[i].username = user.username
    articles[i].avatar_url = user.avatar_url
    articles[i].category_name = articleCategory.name
  }
  console.log(`getting articles 3`)

  return {
    articles: articles, total: total, success: true
  }
}
articleSchema.statics.addArticle = async function (params) {
  console.log(`pramas are ${JSON.stringify(params)}`)
  var newArticle = await this.create(params)
  console.log(`articel is ${JSON.stringify(newArticle)}`)
  await newArticle.save()
  return { success: true, msg: 'success' }
}
module.exports = mongoose.model('Article', articleSchema)
