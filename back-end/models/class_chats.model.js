/* eslint-disable no-undef */
const mongoose = require('mongoose'),
  Schema = mongoose.Schema
var success = true, msg = 'success'

let classChatsSchema = new Schema({
  user_id: { type: String },
  class_id: { type: String },
  user_head_img_url: { type: String },
  username: { type: String },
  message_content: { type: String },
  created_at: { type: Date, default: Date.now() }
})

classChatsSchema.post('save', function () {
  // go emit to the socket
  // socket.broadcast.emit('studentLeaveClass', data)
})

classChatsSchema.statics.getClassChats = async function (params) {
  var classMessages = await this.find({ class_id: params.class_id }).sort({ created_at: 1 })
  return { success: true, classChats: classMessages }
}

classChatsSchema.statics.addClassMsg = async function (params) {
  var newMessage = await this.create(params)
  newMessage.save()
  return { success: success, msg: msg }
}

const classChatsModel = mongoose.model('classClass', classChatsSchema)

module.exports = classChatsModel
