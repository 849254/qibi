/* eslint-disable no-undef */
const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  // PptConverter = require('ppt-png'),
  Converter = require('../converter'),
  fs = require('fs-extra'),
  globals = require('../global')

var success = true, msg = 'success'

let courseClassSchema = new Schema({
  name: { type: String },
  description: { type: String },
  course_id: { type: String },
  class_file_url: { type: String },
  slides_generated: { type: Boolean, default: false },
  video_recorded: { type: Boolean, default: false },
  slides_count: { type: Number, default: 10 }
})

courseClassSchema.methods.generateClassSlides = async function (theClass) {
  console.clear()
  console.log(`Generating Class Slides`)
  var outDir = globals.slidesPath + this._id + '/'
  await new Converter({
    files: [globals.uploadPath + this.class_file_url],
    output: outDir,
    fileNameFormat: '%d.png',
    callback: function (data) {
      console.log(`###########################`)
      theClass.slides_generated = true
      // var files = fs.readdirSync(outDir)
      // var files = fs.scandirSync(outDir)
      // theClass.slides_count = files.length - 1
      // theClass.save()
    }
    // console.log(data.failed, data.success, data.files, data.time)

  }).run()
}

courseClassSchema.pre('save', function () {
  console.log(`pre Save Course Class Triggered`)
  var theClass = this
  if (theClass.isModified('class_file_url')) {
    fs.ensureDirSync(globals.slidesPath + this._id)
    console.log(`The Class File Was Modified`)
    theClass.generateClassSlides(this)
  }
})

courseClassSchema.methods.getClassPath = function (candidatePassword) {

}

courseClassSchema.statics.addClass = async function (params) {
  console.log(`from model :${JSON.stringify(params)}`)
  var theClass = await this.create(params)
  await theClass.save()
  return { success: success, msg: msg }
  /* /params = {
      course_id: this.selectedCourseId,
      name: this.class_name,
      description: this.class_description,
      img_url: this.course_img_url
    }/ */
}

const courseClassModel = mongoose.model('CourseClass', courseClassSchema)

module.exports = courseClassModel
