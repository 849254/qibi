const mongoose = require('mongoose')
let contentSchema = new mongoose.Schema({
  name: { type: String },
  page: { type: String },
  obj: { type: Object }
})
contentSchema.statics.updateAboutIntro = async function (params) {
  await this.updateOne({ name: 'intro' }, {
    obj: {
      Content: params.intro,
      URL: params.url
    }
  })
}
contentSchema.statics.updatePrivacyPolicy = async function (params) {
  await this.updateOne({ name: 'privacyPolicy' }, { obj: { privacyPolicy: params } })
}

contentSchema.statics.updateTermsOfService = async function (params) {
  await this.updateOne({ name: 'termsOfService' }, {
    obj: { termsOfService: params }
  })
}

contentSchema.statics.updateMileStones = async function (params) {
  await this.updateOne({ name: 'milestones' }, { obj: { Array: JSON.parse(JSON.stringify(params)) } })
  console.log(`the params are ${JSON.stringify(params)}`)
}

contentSchema.statics.updateTeamMembers = async function (params) {
  await this.updateOne({ name: 'team' }, { obj: { Team: JSON.parse(JSON.stringify(params)) } })
  console.log(`the params are ${JSON.stringify(params)}`)
}
contentSchema.statics.doLog = async function (params) {
  var logs = await this.findOne({ name: 'logs' })
  logs = logs.obj.logs
  logs.push(params)
  await this.updateOne({ name: 'logs' }, { obj: { logs: logs } })
}

contentSchema.statics.doGetLogs = async function (params) {
  var logs = await this.findOne({ name: 'logs' })
  return logs.obj.logs
}

contentSchema.statics.saveSlides = async function (params) {
  await this.updateOne({ name: 'caroselSlides' }, {
    obj: { caroselSlides: params.slides }
  })
}

module.exports = mongoose.model('Content', contentSchema)
