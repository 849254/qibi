const mongoose = require('mongoose'),
  Schema = mongoose.Schema
var success, msg

let coursCategorySchema = new Schema({
  name: { type: String },
  course_count: { type: Number }
})

coursCategorySchema.pre('save', function (next) {
  next()
})

coursCategorySchema.methods.somFunc = function (candidatePassword) {

}

coursCategorySchema.statics.getCoursesClasses = async function () {
  return {}
}

const courseCategoryModel = mongoose.model('CourseCategory', coursCategorySchema)

module.exports = courseCategoryModel
