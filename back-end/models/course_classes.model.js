/* eslint-disable no-undef */
const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  // PptConverter = require('ppt-png'),
  Converter = require('../converter'),
  fs = require('fs-extra'),
  globals = require('../global')

var success = true, msg = 'success'

let courseClassSchema = new Schema({
  name: { type: String },
  description: { type: String },
  course_id: { type: String },
  class_file_url: { type: String },
  slides_generated: { type: Boolean, default: false },
  video_recorded: { type: Boolean, default: false },
  slides_count: { type: Number, default: 10 }
})
function sleep (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}
courseClassSchema.methods.generateClassSlides = async function () {
  console.clear()
  console.log(`Generating Class Slides`)
  var outDir = globals.slidesPath + this._id + '/'
  let theClass = this
  await new Converter({
    files: [globals.uploadPath + this.class_file_url],
    output: outDir,
    fileNameFormat: '%d.png',
    callback: async function (data) {
      var arr = []
      do {
        arr = fs.readdirSync(outDir)
        await sleep(3000)
      } while (arr.length <= 1)
      theClass.slides_generated = true
      theClass.slides_count = arr.length - 1
      theClass.save()
    }
    // console.log(data.failed, data.success, data.files, data.time)
  }).run()
}

courseClassSchema.pre('save', function () {
  var theClass = this
  if (theClass.isModified('class_file_url')) {
    fs.ensureDirSync(globals.slidesPath + this._id)
    theClass.generateClassSlides()
  }
})

courseClassSchema.methods.getClassPath = function (candidatePassword) {

}

courseClassSchema.statics.addClass = async function (params) {
  console.log(`from model : ${JSON.stringify(params)}`)
  var theClass = await this.create(params)
  await theClass.save()
  return { success: success, msg: msg }
  /* /params = {
    course_id: this.selectedCourseId,
    name: this.class_name,
    description: this.class_description,
    img_url: this.course_img_url
  }/ */
}
courseClassSchema.statics.endClass = async function (params) {
  console.log(`params are ${JSON.stringify(params)}`)
  var theClass = await this.findOne({ _id: params.class_id })
  console.log(`theClass ${theClass}`)
  theClass.video_recorded = true
  await theClass.save()
  return { success: success, msg: msg }
}

const courseClassModel = mongoose.model('CourseClass', courseClassSchema)

module.exports = courseClassModel
