const mongoose = require('mongoose'),
  Schema = mongoose.Schema
var success, msg

let courseSchema = new Schema({
  name: { type: String },
  is_approved: { type: Boolean, default: false },
  category_id: { type: String },
  description: { type: String },
  img_url: { type: String },
  tutor_id: { type: String },
  classes: { type: Array },
  students: { type: Array }
})

courseSchema.pre('save', function (next) {
  next()
})

courseSchema.statics.enroll = async function (params) {
  await this.updateOne({ '_id': params.course_id }, { '$push': { 'students': params.user_id } })
  // implement notifications
}
courseSchema.statics.leave = async function (params) {
  await this.updateOne({ '_id': params.course_id }, { '$pull': { 'students': params.user_id } })
  // implement notifications
}
courseSchema.statics.getHotCourses = async function (params) {
  var courses = await this.find({})
  console.log(`courses are ${courses}`)
  return courses
}

courseSchema.statics.getCourseDetails = async function (id) {
  var course = await this.findOne({ _id: id })
  return course
}

courseSchema.statics.addCourse = async function (params) {
  var course = await this.create(params)
  await course.save()
  success = true
  msg = '成功'
  return { success: success, msg: msg }
}

courseSchema.statics.getMyCourses = async function (tutorId) {
  var courses = await this.find({ tutor_id: tutorId })
  return courses
}

const courseModel = mongoose.model('Course', courseSchema)

module.exports = courseModel
