const mongoose = require('mongoose'),
  Schema = mongoose.Schema
var success, msg

let enrollmentSchema = new Schema({
  course_id: { type: String },
  user_id: { type: String }
})

enrollmentSchema.pre('save', function (next) {
  next()
})

enrollmentSchema.methods.somFunc = function (candidatePassword) {

}

enrollmentSchema.statics.somFunc = async function (params) {
  return {}
}

const enrollmentModel = mongoose.model('Enrollment', enrollmentSchema)

module.exports = enrollmentModel
