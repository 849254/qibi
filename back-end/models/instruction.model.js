const mongoose = require('mongoose')
let instructionnSchema = new mongoose.Schema({
  title: { type: String },
  group: { type: String },
  content: { type: String }
})

module.exports = mongoose.model('instruction', instructionnSchema)
