const mongoose = require('mongoose'),
  Schema = mongoose.Schema
var success, msg

let notificationSchema = new Schema({
  text: { type: String },
  user_id: { type: String },
  is_push_notice: { type: Boolean, default: false },
  avatar_name_url: { type: String },
  link: { type: String },
  type: { type: String },
  img_url: { type: String },
  is_read: { type: Boolean, default: false },
  created_at: { type: Date, default: Date.now() }
})

notificationSchema.methods.somFunc = function (candidatePassword) {

}

notificationSchema.statics.addNotification = async function (params) {
  var notification = await this.create(params)
  await notification.save()
  // success = true
  // msg = '成功'
  // return { success: success, msg: msg }
}

notificationSchema.statics.readNotification = async function (params) {
  var notices = await this.find({ user_id: params.user_id, is_read: false })
  for (var i = 0; i < notices.length; i++) {
    const notice = notices[i]
    notice.is_read = true
    notice.save()
  }
  return true
}

notificationSchema.statics.getAllMyNotifications = async function (userId) {
  var notifications = await this.find({ user_id: userId })
  return notifications
}

notificationSchema.statics.getMyNewNotifications = async function (userId) {
  var notifications = await this.find({ user_id: userId })
  return notifications
}

const notificationModel = mongoose.model('Notification', notificationSchema)

module.exports = notificationModel
