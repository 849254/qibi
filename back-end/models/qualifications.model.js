const mongoose = require('mongoose'),
  Schema = mongoose.Schema
var success, msg

let qualificationSchema = new Schema({
  tutor_id: { type: String },
  type: { type: String }, // accomplishment, diploma, bachelor_degree, master_degree, phd
  institiuation_name: { type: String },
  institiuation_country: { type: String },
  institiuation_state: { type: String },
  institiuation_city: { type: String },
  institiuation_contact_information: { type: String },
  date_of_aquiry: { type: Date },
  scanned_pictures_urls: { type: Array },
  is_deleted: { type: Boolean, default: false }
})

qualificationSchema.pre('save', function (next) {
  next()
})

qualificationSchema.methods.somFunc = function (candidatePassword) {

}

qualificationSchema.statics.addQualification = async function (params) {
  var qualifciation = await this.create(params)
  await qualifciation.save()
  success = true
  msg = '成功'
  return { success: success, msg: msg }
}

const qualificationModel = mongoose.model('Qualification', qualificationSchema)

module.exports = qualificationModel
