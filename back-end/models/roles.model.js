const mongoose = require('mongoose'),
  Schema = mongoose.Schema

let roleSchema = new Schema({
  roleName: { type: String },
  roleCode: { type: Number }
})

module.exports = mongoose.model('Role', roleSchema)
