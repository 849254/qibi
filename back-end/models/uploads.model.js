const mongoose = require('mongoose')

let uploadSchema = new mongoose.Schema({
  userID: { type: String },
  subID: { type: String },
  uploadID: { type: String },
  uploadDir: { type: String },
  fileName: { type: String },
  url: { type: String },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})

uploadSchema.statics.getUploadById = async function (id) {
  console.log(`getUploadById ${id}`)
  var upload = await this.findOne({ uploadID: id })
  return upload
}

uploadSchema.statics.addUpload = async function (params) {
  var newUpload = await this.create(params)
  await newUpload.save()
  return { success: true, msg: 'success' }
}

module.exports = mongoose.model('Upload', uploadSchema)
