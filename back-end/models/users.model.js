const mongoose = require('mongoose'),
  bcrypt = require('bcrypt'),
  Schema = mongoose.Schema,
  phoneToken = require('generate-sms-verification-code'),
  Notifcation = require('./notifications.model'),
  Global = require('../global'),
  fse = require('fs-extra')

var success, msg

let userSchema = new Schema({
  identifer: { type: String },
  username: { type: String },
  password: { type: String },
  role: { type: Number },
  avatar_url: { type: String },
  phone: { type: String },
  email: { type: String },
  // tutor
  is_approved: { type: Boolean, default: false },
  is_new: { type: Boolean, default: true },
  courses: { type: Array },
  class_count: { type: Number, default: 0 },
  course_count: { type: Number, default: 0 },
  driven_income: { type: Number, default: 0 },
  personal_income: { type: Number, default: 0 },
  enrollments: { type: Array },
  enrolled_courses: { type: Array }
})

const SALT_WORK_FACTOR = 10

userSchema.pre('save', function (next) {
  var user = this
  if (user.is_new) {
    console.log(`user id new with id ${user.id}`)
    user.is_new = false
    user.identifer = user.id
    console.log(`x user id new with id ${user.identifer}`)

    var userUploadPath = `${Global.uploadPath}/${user.id}`
    if (!fse.existsSync(userUploadPath)) {
      fse.mkdirSync(userUploadPath)
    }
  }

  if (user.isModified('password')) {
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
      if (err) return next(err)
      bcrypt.hash(user.password, salt, function (err, hash) {
        if (err) return next(err)
        user.password = hash
        next()
      })
    })
  } else if (user.isModified('is_approved')) {
    var params = {
      user_id: user._id,
      text: user.is_approved ? 'you_are_qualified_now' : 'you_are_not_qualified'
    }
    Notifcation.addNotification(params)
    next()
  } else {
    return next()
  }
})

// userSchema.post('save', function (next) {
// var user = this
// next()
// })

userSchema.methods.authenticate = function (candidatePassword) {
  return bcrypt.compareSync(candidatePassword, this.password)
}

userSchema.statics.doLogin = async function (params) {
  console.log(`loging in with params ${params} `)
  success = false
  msg = '用户名或密码有误'
  var path, roleId, userId, avatarUrl, uname = ''
  var user = await this.findOne({ username: params.username })
  // console.log(`user is ${JSON.stringify(user)} `)
  if (!user) {
    user = await this.findOne({ phone: params.username })
  }
  if (user) {
    // console.log(`user is ${JSON.stringify(user)} `)
    success = user.authenticate(params.password)
    if (success) {
      msg = '欢迎光临'
      if (user.role === 1) {
        path = '/dashboard'
      } else {
        path = '/personalcenter'
      }
      roleId = user.role
      userId = user.id
      avatarUrl = user.avatar_url
      uname = user.username
    }
  }
  return { msg: msg, path: path, success: success, role_id: roleId, user_id: userId, username: uname, avatar_url: avatarUrl }
}

userSchema.statics.getMyEnrolledCourse = async function (userId) {
  var courses = await this.find({ user_id: userId }).enrolled_courses
  console.log(`courses are ${courses} `)
  return courses
}

userSchema.statics.doGetTutors = async function () {
  // return tutors
}

userSchema.statics.verifyUsername = async function (username) {
  var users = await this.find({ username: username })
  success = users.length === 0
  return { success: success }
}

userSchema.statics.verifyPhonenumber = async function (phone) {
  var users = await this.find({ phone: phone })
  success = users.length === 0
  return { success: success }
}

userSchema.statics.signup = async function (params) {
  if (params.role === 1) {
    success = false
    msg = '失败'
  } else {
    var user = await this.create(params)
    await user.save()
    success = true
    msg = '成功'
  }
  return { success: success, msg: msg }
}
const userModel = mongoose.model('User', userSchema)

module.exports = userModel
