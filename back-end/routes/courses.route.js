const express = require('express')
const router = express.Router()

const coursesController = require('../controllers/course.controller')
// personal center home
router.get('/getHotCourses', coursesController.getHotCourses)
router.get('/getMyCoursesStats', coursesController.getMyCoursesStats)
router.get('/getMyEnrollmentsStats', coursesController.getMyEnrollmentsStats)
router.get('/getMyEnrollmentsAndNewCourses', coursesController.getMyEnrollmentsAndNewCourses)
router.get('/getCourseDetails', coursesController.getCourseDetails)

// my courses
router.get('/getMyEnrollments', coursesController.getMyEnrollments)
router.get('/getMyCourses', coursesController.getMyCourses)
router.get('/doGetRecentClasses', coursesController.doGetRecentClasses)
router.get('/doGetCourseClasses', coursesController.doGetCourseClasses)
router.get('/doGetCourseInit', coursesController.doGetCourseInit)
router.get('/getCourseCategories', coursesController.getCourseCategories)
router.get('/doGetCourseClassById', coursesController.doGetCourseClassById)

router.get('/getClassChats', coursesController.getClassChats)
router.post('/sendClassMsg', coursesController.sendClassMsg)

// tutor actions
router.post('/addNewCourse', coursesController.addNewCourse)
router.post('/doGenerateClassSlide', coursesController.doGenerateClassSlide)
router.post('/addNewClass', coursesController.addNewClass)
router.post('/updateCourse', coursesController.updateCourse)
router.post('/updateClass', coursesController.updateClass)
router.post('/startNewClass', coursesController.startNewClass)
router.post('/pauseClass', coursesController.pauseClass)
router.post('/endClass', coursesController.endClass)
router.post('/removeClass', coursesController.removeClass)
router.get('/getMyFinacials', coursesController.getMyFinacials)
// student action
router.post('/changeClassTo', coursesController.changeClassTo) // course class section
router.post('/sendClassMsg', coursesController.sendClassMsg)
router.post('/payForClass', coursesController.payForClass)
router.post('/enrollForCourse', coursesController.enrollForCourse)
router.post('/leaveCourse', coursesController.leaveCourse)
router.get('/getMyTimeTable', coursesController.getMyTimeTable)

module.exports = router
