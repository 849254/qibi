// var path = require("path");
const express = require('express')
const router = express.Router()

const dashboardController = require('../controllers/dashboard.controller')
// home
router.get('/doGetUserStats', dashboardController.doGetUserStats)
router.get('/doGetFinacialStats', dashboardController.doGetFinacialStats)
router.get('/doGetCourseStats', dashboardController.doGetCoursesStat)
// email
router.post('/doGetMyEmails', dashboardController.doGetMyEmails)
router.post('/sendEmail', dashboardController.sendEmail)
router.post('/composeEmail', dashboardController.composeEmail)
router.post('/deleteEmail', dashboardController.deleteEmail)
// users
router.get('/doGetUsers', dashboardController.doGetUsers)
router.post('/doDeleteUser', dashboardController.doDeleteUser)
router.post('/doBanUser', dashboardController.doBanUser)
// tutors
router.get('/doGetTutors', dashboardController.doGetTutors)
router.get('/doGetTutorQualifications', dashboardController.doGetTutorQualifications)
router.post('/toggolTutorQualification', dashboardController.toggolTutorQualification)
// router.post('/doApproveTutor', dashboardController.doApproveTutor)
// router.post('/doRevokeTutor', dashboardController.doRevokeTutor)
// courses
router.get('/doGetCourses', dashboardController.doGetCourses)
router.post('/toggoleCourseAproval', dashboardController.toggoleCourseAproval)
// finacial management
router.get('/doGetPaymentRecords', dashboardController.doGetPaymentRecords)
router.get('/doGetPaymentsStats', dashboardController.doGetPaymentsStats)
// pages
router.get('/doGetPageContent', dashboardController.doGetPageContent)
router.post('/doUpdatePageContent', dashboardController.doUpdatePageContent)
// services
// // sms
router.get('/doGetSmsRecords', dashboardController.doGetSmsRecords)
router.get('/doGetSmsStats', dashboardController.doGetSmsStats)
// // wechat
router.get('/doGetWechatSettings', dashboardController.doGetWechatSettings)
router.post('/doSaveWechatSettings', dashboardController.doSaveWechatSettings)
router.get('/doGetWechatMenu', dashboardController.doGetWechatMenu)
router.post('/doSaveWechatMenu', dashboardController.doSaveWechatMenu)

module.exports = router
