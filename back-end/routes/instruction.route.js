const express = require('express')
const router = express.Router()

const instructionController = require('../controllers/instruction.controller')

router.get('/getAllInstruction', instructionController.getAllInstruction)
router.get('/getInstruction', instructionController.getInstruction)
router.post('/updateInstruction', instructionController.updateInstruction)

module.exports = router
