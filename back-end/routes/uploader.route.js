const express = require('express'),
  router = express.Router(),
  uploaderController = require('../controllers/uploader.controller')
router.post('/uploadFile', uploaderController.uploadFile)
router.get('/getUploadById', uploaderController.getUploadById)
module.exports = router
