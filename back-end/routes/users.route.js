const express = require('express'),
  router = express.Router()//,
// security = require('../security')
const userController = require('../controllers/user.controller')
router.get('/getSession', userController.getSession)
router.post('/doLogin', userController.doLogin)
router.post('/doLogout', userController.doLogout)
router.post('/doVerifyName', userController.doVerifyName)
router.post('/doSignup', userController.doSignup)
router.post('/doRest', userController.doRest)
router.post('/doVerifyPhone', userController.doVerifyPhone)
router.post('/doSendCode', userController.doSendCode)

// personal center
// basic information
router.get('/doGetMyPersonalInfo', userController.doGetMyPersonalInfo)
router.post('/doChangePassword', userController.doChangePassword)
router.post('/doSavePersonalInfo', userController.doSavePersonalInfo)
// password and phone change and others to be added here

// tutor qualifications
router.get('/doGetMyQualifications', userController.doGetMyQualifications)
router.post('/doAddQualification', userController.doAddQualification)
router.post('/doDeleteQualification', userController.doDeleteQualification)
router.post('/doUpdateQualification', userController.doUpdateQualification)
router.post('/isQualifiedTutor', userController.isQualifiedTutor)

// messages
router.post('/doMessageUser', userController.doMessageUser)
router.get('/doGetMessages', userController.doGetMessages)
// router.get('/doGetNotifications', userController.doGetNotifications)
// router.get('/doGetAlerts', userController.doGetAlerts)
// notifications
router.post('/addNotification', userController.addNotification)
router.post('/readNotification', userController.readNotification)
router.get('/getAllMyNotifications', userController.getAllMyNotifications)
router.get('/getMyNewNotifications', userController.getMyNewNotifications)
module.exports = router
