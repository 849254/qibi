
const express = require('express')
const router = express.Router()

const wechatController = require('../controllers/wechat.controller')
// router.post('/pay', wechatController.pay)
// router.post('/nativePay', wechatController.nativePay)
router.post('/testApi', wechatController.testApi)
module.exports = router
