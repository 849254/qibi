/* eslint-disable no-unused-expressions */
/* eslint-disable standard/no-callback-literal */
/* eslint-disable handle-callback-err */
console.clear()

const
  globals = require('./global'),
  express = require('express'),
  app = express(),
  port = 5000,
  history = require('connect-history-api-fallback'),
  session = require('express-session'),
  throttle = require('express-throttle-bandwidth'),
  path = require('path'),
  DB = require('./database'),
  kurentoApi = require('./api/kurento.api'),
  // ws = require('ws'),
  // minimist = require('minimist'),
  // https = require('https'),
  // multer = require('multer'),
  server = require('http').Server(app),
  io = require('socket.io')(server)

var idCounter = 0
// var candidatesQueue = {}
// var kurentoClient = null
// var presenter = null
// var viewers = []
// var noPresenterMessage = 'No active presenter. Try again later...'
var db = new DB()
db.init()
// require('./routes/user.route')(app, db);

/* var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    var uploadPath = `${globals.uploadPath}/${req.query.user_id}`
    cb(null, uploadPath)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '_' +  file.fieldname)
  }
}) */

// app.use(multer({ storage: storage }).any())
// app.use(multer({ dest: uploadFolder }))
app.use(throttle(1024 * 128)) // throttling bandwidth
app.use(history())
app.use(express.json({ limit: '10000kb' }))
app.use(express.urlencoded({ limit: '10000kb', extended: true }))
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  // res.header('Access-Control-Allow-Credentials', 'true')
  next()
})

app.use(session({
  secret: 'wkfhwifiu3gr2il3ug2qw',
  resave: true,
  saveUninitialized: true
}))

const dashboard = require('./routes/dashboard.route')
const pages = require('./routes/pages.route')
const users = require('./routes/users.route')
const courses = require('./routes/courses.route')
const wechat = require('./routes/wechat.route')
const contents = require('./routes/contents.route')
const uploader = require('./routes/uploader.route')
const instruction = require('./routes/instruction.route')
app.use('/uploader', uploader)
app.use('/contents', contents)
app.use('/courses', courses)
app.use('/dashboard', dashboard)
app.use('/pages', pages)
app.use('/users', users)
app.use('/wechat', wechat)
app.use('/instruction', instruction)
// Handle production
// if (process.env.NODE_ENV === 'production') {
// Static folder
app.use(express.static(path.join(__dirname, '/../dist/spa')))
app.use(express.static(path.join(__dirname, '/../dist/spa/statics')))
app.use(express.static(path.join(__dirname, '/public')))
// Handle SPA

app.get(/.*/, (req, res) => res.sendFile(path.join(__dirname, '/../dist/spa/index.html')))

server.listen(port)
var config = {
  kms_uri: 'ws://172.16.188.135:8888/kurento'
  // kms_uri: 'ws://192.168.1.103:8888/kurento'
}
kurentoApi.MediaPlayer.init(config)
function nextUniqueId () {
  idCounter++
  return idCounter.toString()
}

io.on('connection', function (socket) {
  // var sessionId = null
  // var request = ws.upgradeReq
  /* var response = {
    writeHead: {}
  } */

  /* sessionHandler(request, response, function (err) {
    sessionId = request.session.id
    console.log('Connection received with sessionId ' + sessionId)
  }) */
  var sessionId = nextUniqueId() // or from the socket get user_id
  console.log(`recived new socket connection : ${sessionId}`)

  socket.on('student_join_class', function (data) {
    console.log('1 - student_join_class :' + JSON.stringify(data))
    socket.broadcast.emit('studentJoinClass', data)
    // socket.broadcast.emit('onSDP', { xxx: 'xxx' })
  })
  socket.on('student_leave_class', function (data) {
    console.log('2 - student_leave_classe')
    socket.broadcast.emit('studentLeaveClass', data)
  })
  socket.on('class_chat_message', function (data) {
    console.log('3 - class_chat_message')
    socket.broadcast.emit('classChatMessage', data)
  })
  socket.on('change_class_slide', function (data) {
    // console.log('changing slide socket reqest to brodcast recived ')
    socket.broadcast.emit('changeClassSlide', data)
  })
  socket.on('tutor_start_class', function (data) {
    // console.log('tutor start class')
    socket.broadcast.emit('tutorStartClass', data)
  })
  socket.on('tutor_end_class', function (data) {
    // console.log('tutor end class')
    socket.broadcast.emit('tutorEndClass', data)
  })
  socket.on('error', function (error) {
    // console.log('streaming error')
    console.log('4 - Connection ' + sessionId + ' error')
    kurentoApi.MediaPlayer.stop(sessionId)
  })

  socket.on('close', function () {
    // console.log('streaming close')
    // console.log('Connection ' + sessionId + ' closed')
    kurentoApi.MediaPlayer.stop(sessionId)
  })

  socket.on('message', function (_message) {
    var message = JSON.parse(_message)
    console.log('Connection ' + sessionId + ' received message ', message)
  })

  socket.on('vplayer', function (message) {
    // var parsedMessage = JSON.parse(message.data);

    console.log(`0 - vplayer msg ${message.id} from `)
    /* socket.broadcast.to(id).emit('answer', {
      a: 'xxx'
    }) */
    switch (message.id) {
      case 'start':
        console.log('msg start')
        var videoUrlToPlay = `file://${globals.classVideosPath}${message.videourl}.webm`
        console.log(`url ${videoUrlToPlay}`)

        kurentoApi.MediaPlayer.start(sessionId, socket, message.sdpOffer, function (error, sdpAnswer) {
          if (error) {
            return socket.emit('socket',
              {
                id: 'error',
                message: error
              })
          }
          // console.log('-------- SDP Answer to ' + id)
          socket.emit('startResponse', {
            sdpAnswer: sdpAnswer
          })
        }, videoUrlToPlay)
        break
      case 'stop':
        console.log('msg stop')
        kurentoApi.MediaPlayer.stop(sessionId)
        break
      case 'pause':
        console.log('msg pause')
        kurentoApi.MediaPlayer.pause(sessionId)
        break
      case 'resume':
        console.log('msg resume')
        kurentoApi.MediaPlayer.resume(sessionId)
        break
      case 'seek':
        console.log('msg seek')
        kurentoApi.MediaPlayer.seek(sessionId, parseInt(message.newPosition))
        break
      case 'onIceCandidate':
        console.log('msg onIceCandidate')
        kurentoApi.MediaPlayer.onIceCandidate(sessionId, message.candidate)
        break
      case 'getPosition':
        console.log('msg getPosition')
        var position = kurentoApi.MediaPlayer.getPosition(sessionId)
        socket.emit('position', {
          id: 'position',
          position: position._id
        })
        break
      default:
        socket.emit('error', {
          id: 'error',
          message: 'Invalid message ' + message
        })
        break
    }
  })
  /* socket.on('message', function (_message) {
    // console.log('wrtc message')
    var message = JSON.parse(_message)
    // console.log('Connection ' + sessionId + ' received message ', message)
    // console.log('Connection : ' + message.id)
    switch (message.id) {
      case 'presenter':
        startPresenter(sessionId, message.sdpOffer, function (error, sdpAnswer) {
          if (error) {
            return socket.broadcast.emit('WRTCSocket', JSON.stringify({
              id: 'presenterResponse',
              response: 'rejected',
              message: error
            }))
          }
          socket.broadcast.emit('WRTCSocket', JSON.stringify({
            id: 'presenterResponse',
            response: 'accepted',
            sdpAnswer: sdpAnswer
          }))
        })
        break

      case 'viewer':
        startViewer(sessionId, socket, message.sdpOffer, function (error, sdpAnswer) {
          if (error) {
            return socket.broadcast.emit('WRTCSocket', JSON.stringify({
              id: 'viewerResponse',
              response: 'rejected',
              message: error
            }))
          }

          socket.broadcast.emit('WRTCSocket', JSON.stringify({
            id: 'viewerResponse',
            response: 'accepted',
            sdpAnswer: sdpAnswer
          }))
        })
        break

      case 'stop':
        stop(sessionId)
        break

      case 'onIceCandidate':
        onIceCandidate(sessionId, message.candidate)
        break

      default:
        socket.broadcast.emit('WRTCSocket', JSON.stringify({
          id: 'error',
          message: 'Invalid message ' + message
        }))
        break
    }
  }) */
})
/* /var server = http.createServer({
    //key: fs.readFileSync(path.join(__dirname, 'keys/key.key')),
    //cert: fs.readFileSync(path.join(__dirname, 'keys/crt.crt'))
}, app).listen(port, function () {
        console.log(`We have started our server on port ${port}`);
})/ */

/* var server = app.listen(port, function () {
  console.log(`We have started our server on port ${port}`)
})
var argv = minimist(process.argv.slice(2), {
  default: {
    as_uri: 'https://localhost:8443/--',
    // ws_uri: 'ws://localhost:8888/kurento'
    ws_uri: 'ws://192.168.1.101:8888/kurento'
  }
}) */

// var asUrl = url.parse(argv.as_uri)

/* var options = {
var PptConverter = require('ppt-png')
new PptConverter({
  files: [globals.uploadPath + '/sample.ppt'],
  output: globals.slidesPath, // + parmas.class_id,
  callback: function (data) {
    console.log(data.failed, data.success, data.files, data.time)
  }
}).run()
    // key: fs.readFileSync('keys/privkey1.pem'),
    // cert: fs.readFileSync('keys/cert1.pem')
}; */

/* var socketServer = https.createServer(options, app).listen(8443, function () {
}); */

/* var wss = new ws.Server({
  server: server,
  path: '/streaming'
}) */

/*
 * Management of WebSocket messages
 */

/* wss.on('connection', function (ws) {
  var sessionId = nextUniqueId()
  console.log('Connection received with sessionId ' + sessionId)

  ws.on('error', function (error) {
    console.log('Connection ' + sessionId + ' error')
    stop(sessionId)
  })

  ws.on('close', function () {
    console.log('Connection ' + sessionId + ' closed')
    stop(sessionId)
  })

  ws.on('message', function (_message) {
    var message = JSON.parse(_message)
    console.log('Connection ' + sessionId + ' received message ', message)

    switch (message.id) {
      case 'presenter':
        startPresenter(sessionId, ws, message.sdpOffer, function (error, sdpAnswer) {
          if (error) {
            return ws.send(JSON.stringify({
              id: 'presenterResponse',
              response: 'rejected',
              message: error
            }))
          }
          ws.send(JSON.stringify({
            id: 'presenterResponse',
            response: 'accepted',
            sdpAnswer: sdpAnswer
          }))
        })
        break

      case 'viewer':
        startViewer(sessionId, ws, message.sdpOffer, function (error, sdpAnswer) {
          if (error) {
            return ws.send(JSON.stringify({
              id: 'viewerResponse',
              response: 'rejected',
              message: error
            }))
          }

          ws.send(JSON.stringify({
            id: 'viewerResponse',
            response: 'accepted',
            sdpAnswer: sdpAnswer
          }))
        })
        break

      case 'stop':
        stop(sessionId)
        break

      case 'onIceCandidate':
        onIceCandidate(sessionId, message.candidate)
        break

      default:
        ws.send(JSON.stringify({
          id: 'error',
          message: 'Invalid message ' + message
        }))
        break
    }
  })
})

/*
 * Definition of functions
 */

// Recover kurentoClient for the first time.
/* function getKurentoClient (callback) {
  if (kurentoClient !== null) {
    return callback(null, kurentoClient)
  }

  kurento(argv.ws_uri, function (error, _kurentoClient) {
    if (error) {
      console.log('Could not find media server at address ' + argv.ws_uri)
      return callback('Could not find media server at address' + argv.ws_uri +
        '. Exiting with error ' + error)
    }

    kurentoClient = _kurentoClient
    callback(null, kurentoClient)
  })
}

function startPresenter (sessionId, socket, sdpOffer, callback) {
  clearCandidatesQueue(sessionId)

  if (presenter !== null) {
    stop(sessionId)
    return callback('Another user is currently acting as presenter. Try again later ...')
  }

  presenter = {
    id: sessionId,
    pipeline: null,
    webRtcEndpoint: null
  }

  getKurentoClient(function (error, kurentoClient) {
    if (error) {
      stop(sessionId)
      return callback(error)
    }

    if (presenter === null) {
      stop(sessionId)
      return callback(noPresenterMessage)
    }

    kurentoClient.create('MediaPipeline', function (error, pipeline) {
      if (error) {
        stop(sessionId)
        return callback(error)
      }

      if (presenter === null) {
        stop(sessionId)
        return callback(noPresenterMessage)
      }

      presenter.pipeline = pipeline
      pipeline.create('WebRtcEndpoint', function (error, webRtcEndpoint) {
        if (error) {
          stop(sessionId)
          return callback(error)
        }

        if (presenter === null) {
          stop(sessionId)
          return callback(noPresenterMessage)
        }

        presenter.webRtcEndpoint = webRtcEndpoint

        if (candidatesQueue[sessionId]) {
          while (candidatesQueue[sessionId].length) {
            var candidate = candidatesQueue[sessionId].shift()
            webRtcEndpoint.addIceCandidate(candidate)
          }
        }

        webRtcEndpoint.on('OnIceCandidate', function (event) {
          var candidate = kurento.getComplexType('IceCandidate')(event.candidate)
          socket.broadcast.emit('WRTCSocket', JSON.stringify({
            id: 'iceCandidate',
            candidate: candidate
          }))
        })

        webRtcEndpoint.processOffer(sdpOffer, function (error, sdpAnswer) {
          if (error) {
            stop(sessionId)
            return callback(error)
          }

          if (presenter === null) {
            stop(sessionId)
            return callback(noPresenterMessage)
          }

          callback(null, sdpAnswer)
        })

        webRtcEndpoint.gatherCandidates(function (error) {
          if (error) {
            stop(sessionId)
            return callback(error)
          }
        })
      })
    })
  })
}

function startViewer (sessionId, socket, sdpOffer, callback) {
  clearCandidatesQueue(sessionId)

  if (presenter === null  added|| presenter.pipeline === null ) {
  stop(sessionId)
  return callback(noPresenterMessage)
}

presenter.pipeline.create('WebRtcEndpoint', function (error, webRtcEndpoint) {
  if (error) {
    stop(sessionId)
    return callback(error)
  }
  viewers[sessionId] = {
    'webRtcEndpoint': webRtcEndpoint,
    'socket': socket
  }

  if (presenter === null) {
    stop(sessionId)
    return callback(noPresenterMessage)
  }

  if (candidatesQueue[sessionId]) {
    while (candidatesQueue[sessionId].length) {
      var candidate = candidatesQueue[sessionId].shift()
      webRtcEndpoint.addIceCandidate(candidate)
    }
  }

  webRtcEndpoint.on('OnIceCandidate', function (event) {
    var candidate = kurento.getComplexType('IceCandidate')(event.candidate)
    socket.broadcast.emit('WRTCSocket', JSON.stringify({
      id: 'iceCandidate',
      candidate: candidate
    }))
  })

  webRtcEndpoint.processOffer(sdpOffer, function (error, sdpAnswer) {
    if (error) {
      stop(sessionId)
      return callback(error)
    }
    if (presenter === null) {
      stop(sessionId)
      return callback(noPresenterMessage)
    }

    presenter.webRtcEndpoint.connect(webRtcEndpoint, function (error) {
      if (error) {
        stop(sessionId)
        return callback(error)
      }
      if (presenter === null) {
        stop(sessionId)
        return callback(noPresenterMessage)
      }

      callback(null, sdpAnswer)
      webRtcEndpoint.gatherCandidates(function (error) {
        if (error) {
          stop(sessionId)
          return callback(error)
        }
      })
    })
  })
})
}

function clearCandidatesQueue (sessionId) {
  if (candidatesQueue[sessionId]) {
    delete candidatesQueue[sessionId]
  }
}

function stop (sessionId) {
  console.log(`stop been called`)
  if (presenter !== null && presenter.id === sessionId) {
    for (var i in viewers) {
      var viewer = viewers[i]
      if (viewer.socket) {
        viewer.socket.broadcast.emit('WRTCSocket', JSON.stringify({
          id: 'stopCommunication'
        }))
      }
      // socket.broadcast.emit('WRTCSocket',
    }
    // added if (presenter.pipeline != null) {
    presenter.pipeline.release()
    // }
    presenter = null
    viewers = []
  } else if (viewers[sessionId]) {
    viewers[sessionId].webRtcEndpoint.release()
    delete viewers[sessionId]
  }

  clearCandidatesQueue(sessionId)

  if (viewers.length < 1 && !presenter) {
    console.log('Closing kurento client')
    // added if (kurentoClient != null) {
    kurentoClient.close()
    // }
    kurentoClient = null
  }
}

function onIceCandidate (sessionId, _candidate) {
  var candidate = kurento.getComplexType('IceCandidate')(_candidate)

  if (presenter && presenter.id === sessionId && presenter.webRtcEndpoint) {
    console.info('Sending presenter candidate')
    presenter.webRtcEndpoint.addIceCandidate(candidate)
  } else if (viewers[sessionId] && viewers[sessionId].webRtcEndpoint) {
    console.info('Sending viewer candidate')
    viewers[sessionId].webRtcEndpoint.addIceCandidate(candidate)
  } else {
    console.info('Queueing candidate')
    if (!candidatesQueue[sessionId]) {
      candidatesQueue[sessionId] = []
    }
    candidatesQueue[sessionId].push(candidate)
  }
} */
